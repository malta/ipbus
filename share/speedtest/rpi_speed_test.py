#!/usr/bin/env python
import sys
import time
import argparse
import time
import Herakles

parser = argparse.ArgumentParser()
parser.add_argument("-V", "--verbose", help="enable verbose mode",action='store_true')
parser.add_argument("-c", "--connstr", help="IPbus connection string", required=True)
parser.add_argument("-o", "--output", help="Output file name")
parser.add_argument("-a", "--addr", help="address", default=0,type=int)
parser.add_argument("-r", "--readonly", help="read only", action="store_true")
parser.add_argument("-n", "--number", help="Number of repetitions", default=100, type=int)
args=parser.parse_args()

#if target == "192.168.0.1":
#    connection="192.168.0.1:50001"
#    testname="Direct connection to FPGA.UDP"
#    pass
#else:
#    connection="tcp://%s:50001?target=192.168.0.1:50001" % target
#    testname="Using raspberrypi. %s" % target
#    pass

print ("Test IPBus speed")
ipb=Herakles.Uhal(args.connstr)
ipb.SetVerbose(args.verbose)

t0 = time.time()
for x in xrange(0,args.number):
    if not args.readonly: ipb.Write(args.addr, x)
    v=ipb.Read(args.addr)
    pass
t1 = time.time()

nt=args.number if args.readonly else 2*args.number
et=t1-t0
print("Number of readings: %i"%args.number)
print("Number of transactions: %i"% nt)
print("Elapsed time [s]: %.3f" % et)
print("Time per transaction [ms]: %.3f" % (1000*et/nt))
print("Have a nice day")














