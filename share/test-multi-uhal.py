#!/usr/bin/env python

import Herakles

ipb=[]
print "Connect 1"
h=Herakles.Uhal("udp://localhost:50001")
ipb.append(h)

print "Connect 1"
h=Herakles.Uhal("udp://localhost:50002")
ipb.append(h)

print "Set Verbose"
for h in ipb:
    h.SetVerbose(False)
    pass

print "Read one value from one register"
for h in ipb:
    print "=> %x" % h.Read(0)
    print ""
    
print "Read 10 values from register"
for h in ipb:
    vs = h.Read(0,10)
    for v in vs:
        print "=> %x" % v
        pass
    print ""
