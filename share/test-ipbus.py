#!/usr/bin/env python
import os
import sys
import argparse
import Herakles

parser=argparse.ArgumentParser()
parser.add_argument('-c','--constr',default='tcp://pcatlidrpi02:50001?target=192.168.0.1:50001')
args=parser.parse_args()

ipb=Herakles.Uhal(args.constr)
ipb.SetVerbose(True)
ipb.Read(0)

#for i in xrange(100):
#    v=ipb.Read(0x7)
#    ipb.Write(0x8,[v,]*8,True)
#    print "0x%X" % v
#    pass
