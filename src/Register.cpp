
#include <ipbus/Register.h>

using namespace std;
using namespace ipbus;

Register::Register(string name, uint32_t address, uint32_t startbit, uint32_t nbits, uint32_t defvalue){
  m_name=name;
  m_address=address;
  m_startbit=startbit & 0x1F;
  m_nbits=nbits & 0x3F;
  m_defvalue=defvalue;
  m_value=defvalue;
  m_mask=0;
  for(uint32_t i=startbit;i<startbit+nbits;i++){
    m_mask|=1<<i;
  }
  m_updated=true;
  m_uhal=0;
}

Register::~Register(){}

void Register::SetUhal(Uhal* uhal){
  m_uhal=uhal;
}

void Register::SetValue(uint32_t value, bool force){
  m_value=value;
  m_updated=true;
  if(force && m_uhal){
    /*
      uint32_t val=0;
      m_uhal->Read(m_address,val);
      val|=~m_mask;
      val|=(m_value<<m_startbit);
      m_uhal->Write(m_address,val);
    */
    m_uhal->RMWB(m_address,value,m_startbit,m_nbits);
    m_updated=false;
  }
}

void Register::SetUpdated(bool updated){
  m_updated=updated;
}

uint32_t Register::GetValue(bool force){
  if(force && m_uhal){
    uint32_t val=0;
    m_uhal->Read(m_address,val);
    m_value=(val&m_mask)>>m_startbit;
  }
  return m_value;
}

bool Register::Updated(){
  return m_updated;
}

string Register::GetName(){
  return m_name;
}

uint32_t Register::GetAddress(){
  return m_address;
}

uint32_t Register::GetStartBit(){
  return m_startbit;
}

uint32_t Register::GetNbits(){
  return m_nbits;
}

uint32_t Register::GetDefault(){
  return m_defvalue;
}

uint32_t Register::GetMask(){
  return m_mask;
}
