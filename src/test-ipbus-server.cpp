/**
 * Test the ipbus server
 * Author: Carlos.Solans@cern.ch
 *
 */
#include <iostream>
#include <cstring>
#include <map>
#include <signal.h>
#include <unistd.h>
#include "ipbus/Server.h"

using namespace std;
using namespace ipbus;

bool g_cont;

void handler(int s){
  cout << "You pressed ctrl+c" << endl;
  g_cont=false;
}

int main(int argc, const char* argv[]){

  for(int i=1;i<argc;i++){
    const char * pos=strstr(argv[i],"-h");
    if(pos==NULL) continue;
    cout << "Usage: " << argv[0] << " [-v] [-p] " << endl
	 << " -v --verbose : enable verbose mode" << endl
	 << " -p --port [number]: port number" << endl;
    return 0;
  }

  bool verbose = false;
  int port = 50001;
  
  for(int i=1;i<argc;i++){
    const char * pos=strstr(argv[i],"-p");
    if(pos==NULL) continue;
    if(strlen(argv[i])>2){
      port = atoi(&argv[i][2]);
    }else{
      port = atoi(argv[i+1]);
    }
  }
  
  for(int i=1;i<argc;i++){
    const char * pos=strstr(argv[i],"-v");
    if(pos==NULL) continue;
    verbose=true;
  }
  
  std::map<uint32_t,uint32_t> mem;
  
  Server * server = new Server(port);
  server->AddRegister(0,
		      [&](){cout<<"READ MEMORY 0: "<<mem[0]<<endl;return mem[0];},
		      [&](uint32_t value){cout<<"WRITE MEMORY 0: "<<value<<endl; mem[0]=value;}
		      );
  
  if(verbose) cout << "Start server" << endl;
  server->Start();
  
  g_cont=true;
  signal(SIGINT,handler);
  
  while(g_cont){usleep(100000);}
  
  if(verbose) cout << "Stop server" << endl;
  server->Stop();
  delete server;
  
  cout << "Have a nice day" << endl;
  return 0;
}
