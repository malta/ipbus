/*******************************
 * Uhal
 * IPBus 2.0 client
 *
 * Carlos.Solans@cern.ch
 ******************************/

#include "ipbus/Uhal.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netdb.h>
#include <iostream>
#include <string.h>
#include <algorithm>
#include <unistd.h>

using namespace ipbus;
using namespace std;

bool Uhal::verbose = false;

Uhal::Uhal(string hostname, uint32_t port){
  synced=false;
  proto=Uhal::UDP;
  ncalls=0;
  struct hostent * host = gethostbyname(hostname.c_str());
  if(host==NULL){
    cout << "Uhal host not found: " << hostname << endl;
    return;
  }
  memcpy(&server.sin_addr, host->h_addr_list[0], host->h_length);
  server.sin_family = AF_INET;
  server.sin_port = htons(port);
  sock = socket(AF_INET,SOCK_DGRAM,0);
  //Disable Nagle's algorithm
  int nonagle = 1;
  setsockopt(sock,IPPROTO_TCP, nonagle, (char *)&nonagle, sizeof(nonagle));
  SetTimeout(100);
  //SetTimeout(3000);
  Sync();
}

Uhal::Uhal(string conn_str){
  synced=false;
  proto=Uhal::UDP;
  ncalls=0;
  string host="localhost";
  uint32_t port=50001;
  string host2="localhost";
  uint32_t port2=50001;
  sock=0;
  
  size_t pos1 = 0;
  size_t pos2 = 0;

  //protocol
  pos2=conn_str.find("://",pos1);
  if(pos2!=string::npos && pos2>pos1){
    if(conn_str.substr(pos1,pos2)=="tcp"){proto=Uhal::TCP;}
    if(verbose) cout << "Protocol => 1: " << conn_str.substr(pos1,pos2) << endl; 
    pos1=pos2+3;
  }
  
  //hostname
  pos2=conn_str.find(":",pos1);
  size_t pos3=conn_str.find("?target",pos1);
  if(pos2!=string::npos){
    host=conn_str.substr(pos1,pos2-pos1);
    pos1=pos2+1;
    if(verbose) cout << "Host => 1: " << host << endl; 
  }else if(pos3!=string::npos){
    host=conn_str.substr(pos1,pos3-pos1);
    pos1=pos3;//to be able to find it again
    if(verbose) cout << "Host => 2: " << host << endl; 
  }else{
    host=conn_str.substr(pos1);
    pos1=conn_str.size();
    if(verbose) cout << "Host => 3: " << host << endl; 
  }

  //port
  pos2=conn_str.find("?target=",pos1);
  if(pos2==string::npos && pos1!=conn_str.size()){
    port=atoi(conn_str.substr(pos1).c_str());
    pos1=conn_str.size();
    if(verbose) cout << "Port => 1: " << port << endl; 
  }else if(pos2!=string::npos && pos2>pos1){
    port=atoi(conn_str.substr(pos1,pos2-pos1).c_str());
    pos1=pos2+8;
    if(verbose) cout << "Port => 2: " << port << endl; 
  }

  //device host
  pos2=conn_str.find(":",pos1);
  if(pos2==string::npos && pos1!=conn_str.size()){
    host2=conn_str.substr(pos1).c_str();
    pos1=conn_str.size();
    if(verbose) cout << "Host2 => 1: " << host2 << endl; 
  }else if(pos2!=string::npos){
    host2=conn_str.substr(pos1,pos2-pos1).c_str();
    pos1=pos2+1;
    if(verbose) cout << "Host2 => 2: " << host2 << endl; 
  }

  //device port
  if(pos1!=conn_str.size()){
    port2=atoi(conn_str.substr(pos1).c_str());
    if(verbose) cout << "Port2 => 1: " << port2 << endl; 
  }

  //Connect to host
  struct hostent * h = gethostbyname(host.c_str());
  if(h==NULL){
    cout << "Uhal host not found: " << host << endl;
    return;
  }

  memcpy(&server.sin_addr, h->h_addr, h->h_length);
  server.sin_family = AF_INET;
  server.sin_port = htons(port);
  
  if(proto==Uhal::UDP){

    cout << "Uhal UDP host=" << host << " port=" << port << endl;
    sock = socket(AF_INET,SOCK_DGRAM,0);
  
  }else if(proto==Uhal::TCP){
	
    cout << "Uhal TCP host=" << host << " port=" << port 
         << " host2=" << host2 << " port2=" << port2 << endl;
	
    sock = socket(AF_INET, SOCK_STREAM, 0);
    preamble = new uint32_t[4];
    h = gethostbyname(host2.c_str());
    memcpy(&devicehost, h->h_addr, h->h_length);
    deviceport = port2;
    if(connect(sock,(struct sockaddr*)&server,sizeof(server)) < 0){
      connected=false;
      cout << "Cannot connect to socket" << endl; 
      return; 
    }
    connected=true;
  }
  //Disable Nagle's algorithm
  int nonagle = 1;
  setsockopt(sock,IPPROTO_TCP, nonagle, (char *)&nonagle, sizeof(nonagle));
  SetTimeout(100);
  //SetTimeout(3000);
  Sync();
}


Uhal::~Uhal(){
  if(connected) close(sock);
}

void Uhal::Sync(){
  Status * p = (Status*)Send(&status);
  if(p==NULL) {synced=false; return;}
  pkid=p->GetNextExpectedPacketId();
  Packet::SetNextPacketId(p->GetNextExpectedPacketId());
  synced=true;
}

bool Uhal::IsSynced(){
  return synced;
}

const Transaction* Uhal::Resync(const Transaction* req){
  Sync();
  resend.SetPkid(req->GetPkid());
  const Transaction * rep = dynamic_cast<const Transaction*>(Send(&resend));
  return rep;
}

const Packet * Uhal::Send(Packet *p){
  const Packet * reply = NULL;
  ncalls++;
  //Add the expected packet id to the packet
  if(p->GetType()==PacketHeader::CONTROL){
    p->SetPkid(pkid);
  }
  //send data
  p->Pack();
  if(verbose){ 
    cout << "Outgoing frame" << endl;
    p->Dump();
  }
  if(proto==Uhal::UDP){
    sendto(sock,p->GetBytes(),p->GetLength(),0,(struct sockaddr*)&server,sizeof(server));
    //receive acknowledge
    int32_t n=recvfrom(sock,pb.GetBytes(),10000,0,NULL,NULL);
    //fast return
    if(n<=0){ return reply; }
    //mark as sent
    p->Sent();
    //increase the expected packet id
    if(p->GetType()==PacketHeader::CONTROL){
      pkid++;
      if(pkid>0xFFFF){pkid=0;}
    }
    //unpack
    pb.SetLength(n);
    pb.Unpack();
    reply = pb.GetPacket();
  }else if(proto==Uhal::TCP){
    if(!connected){return reply;}
    //if(p->GetType()==PacketHeader::STATUS){return reply;}
    if(p->GetType()==PacketHeader::STATUS){return p;}
    //Request preamble
    //0 [31- 0] packet size
    //1 [31- 0] device address 
    //2 [31-16] device port
    //3 [15- 0] number of 32-bit words in the ipbus packet
    //write preamble
    preamble[0] = htonl(p->GetLength()+8);
    preamble[1] = devicehost;
    preamble[2] = htonl((deviceport<<16) | ((p->GetLength()/4)&0xFF));
    int32_t nb = 0;
    nb = write(sock,preamble,12);
    nb = write(sock,p->GetBytes(),p->GetLength());
    //Acknowledge preamble
    //0 [31- 0] packet size
    //1 [31- 0] device address top 
    //2 [31- 0] device address 
    //3 [15- 0] device port
    //3 [31-16] error code
    uint32_t bytecount, deviceaddr2, deviceport2, errorcode;
    nb  = read(sock,preamble,16);
    //quick reply
    if(nb<0){ return reply; }
    bytecount = ntohl(preamble[0]);
    deviceaddr2 = preamble[2];
    deviceport2 = ntohs(preamble[3]&0xFFFF);
    errorcode = ntohs(preamble[3]>>16);
   	if(verbose && false){
      cout << "bytecount: " << bytecount << endl
           << "devhost:   " << std::hex << deviceaddr2 << " " << devicehost << std::dec << endl
           << "devport:   " << std::hex << deviceport2 << " " << deviceport << std::dec << endl
           << "errorcode: " << errorcode << endl;
    }
    if(devicehost!=deviceaddr2){
	    cout << "Error: Reply does not match target address" << endl; 
	    return reply;
	  }
    if(deviceport!=deviceport2){
	    cout << "Error: Reply does not match target port" << endl;
	    return reply;
    }
    switch(errorcode){
      case 0: break; //all ok
      case 1:
      case 3:
      case 4:
        cout << "Error: No response from target" << endl; return reply;
      case 2:
        cout << "Error: Timeout from target" << endl; return reply;
      case 5:
        cout << "Malformed status from target" << endl; return reply;
      case 6:
        cout << "Incorrect protocol version" << endl; return reply;
      default:
        cout << "Unknown error code " << errorcode << endl; return reply;
    }
    //Read the rest of the fragment
    nb = read(sock,pb.GetBytes(),bytecount-12);
    //mark as sent
    p->Sent();
    //increase the expected packet id
    if(p->GetType()==PacketHeader::CONTROL){
      pkid++;
      if(pkid>0xFFFF){pkid=0;}
    }
    //set length
    pb.SetLength(bytecount-12);
    //unpack
    pb.Unpack();
    reply = pb.GetPacket();
  }
  if(verbose){
    cout << "Incomming frame" << endl;
    reply->Dump();
  }
  return reply;
}

void Uhal::RMWB(const uint32_t address, const uint32_t mask, const uint32_t value){
  req.Init();
  req.SetRMWB(address,mask,value);
  const Transaction * rep = dynamic_cast<const Transaction*>(Send(&req));
  if(rep==NULL){rep = Resync(&req);}
  if(rep==NULL)return;
}

void Uhal::RMWB(const uint32_t address, const uint32_t value, const uint32_t startbit, const uint32_t nbits){
  req.Init();
  req.SetRMWB(address,value,startbit,nbits);
  const Transaction * rep = dynamic_cast<const Transaction*>(Send(&req));
  if(rep==NULL){rep = Resync(&req);}
  if(rep==NULL)return;
}

void Uhal::Write(const uint32_t address, const uint32_t value){
  req.Init();
  req.SetWrite(address,value);
  const Transaction * rep = dynamic_cast<const Transaction*>(Send(&req));
  if(rep==NULL){rep = Resync(&req);}
  if(rep==NULL)return;
}

void Uhal::Write(const uint32_t address, const uint32_t * values, const uint32_t size, bool fifo){
  req.Init();
  req.SetWrite(address,values,size,fifo);
  const Transaction * rep = dynamic_cast<const Transaction*>(Send(&req));
  if(rep==NULL){rep = Resync(&req);}
  if(rep==NULL)return;
}

void Uhal::Write(const uint32_t address, const vector<uint32_t> & values, bool fifo){
  req.Init();
  req.SetWrite(address,values,fifo);
  const Transaction * rep = dynamic_cast<const Transaction*>(Send(&req));
  if(rep==NULL){rep = Resync(&req);}
  if(rep==NULL)return;
}

void Uhal::Read(const uint32_t address, uint32_t & value){
  req.Init();
  req.SetRead(address,1);
  const Transaction * rep = dynamic_cast<const Transaction*>(Send(&req));
  if(rep==NULL){rep = Resync(&req);}
  if(rep==NULL){value=0; return;}
  if(rep->GetData().size()>0) value = rep->GetData().at(0);
}

void Uhal::Read(const uint32_t address, vector<uint32_t> & values, const uint32_t size, bool fifo){
  req.Init();
  req.SetRead(address,size,fifo);
  const Transaction * rep = dynamic_cast<const Transaction*>(Send(&req));
  if(rep==NULL){rep = Resync(&req);}
  if(rep==NULL){values.clear(); return;}
  if(values.size()!=size){values.resize(size);}
  for(uint32_t i=0;i<rep->GetData().size();i++){
    values[i]=rep->GetData()[i];
  }
}

void Uhal::Read(const uint32_t address, uint32_t * values, const uint32_t size, bool fifo){
  req.Init();
  req.SetRead(address,size,fifo);
  const Transaction * rep = dynamic_cast<const Transaction*>(Send(&req));
  if(rep==NULL){rep = Resync(&req);}
  if(rep==NULL){return;}
  for(uint32_t i=0;i<rep->GetData().size();i++){
    values[i]=rep->GetData()[i];
  }
}

void Uhal::SetVerbose(bool enable){
  verbose = enable;
}

void Uhal::SetTimeout(uint32_t milis){
  struct timeval tv;
  tv.tv_sec = (int)milis/1000;
  tv.tv_usec = (int)(milis%1000)*1000;
  setsockopt(sock,SOL_SOCKET,SO_RCVTIMEO,&tv,sizeof(tv));
}

uint32_t Uhal::GetNumCalls(){
  return ncalls;
}

uint32_t Uhal::GetPacketId(){
  return pkid;
}
