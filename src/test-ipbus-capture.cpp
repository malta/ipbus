#include <iostream>
#include <cstring>
#include <map>
#include <signal.h>
#include <unistd.h>

#include <pcap.h>
#include <netinet/ip.h>
#include <net/ethernet.h>
#include <iomanip>
#include "ipbus/PacketBuilder.h"
#include "ipbus/Transaction.h"

using namespace std;
using namespace ipbus;

pcap_t * g_pcap; 
bool g_verbose;
PacketBuilder * g_pb;

void handler(int s){
  cout << "You pressed ctrl+c" << endl;
  pcap_breakloop(g_pcap);
}

struct ip_hdr_t{
  u_char ihl:4, version:4; 
  uint8_t tos;
  uint16_t len;
  uint16_t id;
  uint16_t flags;
  uint8_t ttl;
  uint8_t protocol;
  uint16_t ip_chk;
  u_char src_h[4];
  u_char dst_h[4];
};

struct udp_hdr_t{
  uint16_t src;
  uint16_t dst;
  uint16_t len;
  uint16_t chk;
};

struct eth_hdr_t{
  u_char src[6];
  u_char dst[6];
  uint16_t extra;
};

struct test_hdr_t{
  uint32_t firstword;
};

#define NO_ETH_HDR 0x00000002

void packetHandler(u_char *userData, const struct pcap_pkthdr* pkthdr, const u_char* packet) {
  cout << "Got new packet" << endl;
  if(g_verbose){
    for(uint32_t i=0; i<pkthdr->len; i++){
      if(i%4==0 and i!=0){cout << endl;}
      cout << hex << setw(2) << setfill('0') << (uint32_t)packet[i] << dec;
    }
    cout << endl;
  }
  
  struct test_hdr_t * test_hdr = (struct test_hdr_t *) (packet);
  struct eth_hdr_t * eth_hdr = (struct eth_hdr_t *) (packet);
    
  uint32_t offset = sizeof(eth_hdr_t);
  if(NO_ETH_HDR==test_hdr->firstword){offset=4;}  
  else{
    cout << "mac src: " << setw(2) << setfill('0') << hex; for(uint32_t i=0;i<6;i++){cout << (uint32_t) eth_hdr->src[i] << ":";} cout << dec << endl; 
    cout << "mac dst: " << setw(2) << setfill('0') << hex; for(uint32_t i=0;i<6;i++){cout << (uint32_t) eth_hdr->dst[i] << ":";} cout << dec << endl; 
  }
  struct ip_hdr_t * ip_hdr = (struct ip_hdr_t *) (packet+offset);
  struct udp_hdr_t * udp_hdr = (struct udp_hdr_t *) (packet+offset+ip_hdr->ihl*4);
  
  if(g_verbose){
    cout << "IPv: " << (uint32_t)ip_hdr->version << endl;
    cout << "IHL: " << (uint32_t)ip_hdr->ihl << endl;
  }
  cout << "ip src: "; for(uint32_t i=0;i<4;i++){cout << (uint32_t) ip_hdr->src_h[i] << ".";} cout << ntohs(udp_hdr->src) << endl; 
  cout << "ip dst: "; for(uint32_t i=0;i<4;i++){cout << (uint32_t) ip_hdr->dst_h[i] << ".";} cout << ntohs(udp_hdr->dst) << endl; 
  offset+=ip_hdr->ihl*4+sizeof(udp_hdr_t);
  
  if(g_verbose){cout << "offset: " << offset << endl;}
  for(uint32_t i=offset; i<pkthdr->len; i++){
    g_pb->GetBytes()[i-offset]=packet[i];
  }
  g_pb->SetLength(pkthdr->len-offset);
  g_pb->Unpack();
  const Packet * p=g_pb->GetPacket();
  const string ptypes[3]={"Transaction","Status","Resend"};
  cout << "Packet type: " << ptypes[p->GetType()] << endl;
  cout << "Packet ID: " << p->GetPkid() << endl;
  if(p->GetType()==PacketHeader::CONTROL){
    Transaction *t = (Transaction*) p;
    const string types[5]={"READ","WRITE","READFIFO","WRITEFIFO","RMWBITS"};
    cout << "Transaction type: " << types[t->GetTransactionHeader()->GetType()] << endl;
    cout << "Transaction flow: " << (t->GetTransactionHeader()->GetInfo()==0XF?"REQUEST":"REPLY") << endl;
  }
  p->Dump();
}


int main(int argc, const char* argv[]){

  for(int i=1;i<argc;i++){
    const char * pos=strstr(argv[i],"-h");
    if(pos==NULL) continue;
    cout << "Usage: " << argv[0] << " [-v] [-p] " << endl
         << " -v : enable verbose mode" << endl
         << " -d : device (eth0)" << endl
         << " -s : server (ipbus-server)" << endl;
    pcap_if_t *interfaces;
    char errmsg[PCAP_ERRBUF_SIZE];
    if(pcap_findalldevs(&interfaces,errmsg)==0){
      cout << "Available Devices are :" << endl;
      for(pcap_if_t * dev = interfaces; dev != NULL ; dev = dev->next){
        cout << dev->name << endl;
      }
    }  
    return 0;
  }

  bool verbose;
  string server("localhost");
  string device("eth0");
  
  for(int i=1;i<argc;i++){
    const char * pos=strstr(argv[i],"-v");
    if(pos==NULL) continue;
    verbose=true;
  }

  for(int i=1;i<argc;i++){
    const char * pos=strstr(argv[i],"-s");
    if(pos==NULL) continue;
    if(strlen(argv[i])>2){
      server = string(&argv[i][2]);
    }else{
      server = string(argv[i+1]);
    }
  }

  for(int i=1;i<argc;i++){
    const char * pos=strstr(argv[i],"-d");
    if(pos==NULL) continue;
    if(strlen(argv[i])>2){
      device = string(&argv[i][2]);
    }else{
      device = string(argv[i+1]);
    }
  }
  
  g_verbose=verbose;  
  string filter = "host "+server+" and udp";
  char errbuf[PCAP_ERRBUF_SIZE];
      
  g_pcap = pcap_open_live(device.c_str(), BUFSIZ, 1, 1, errbuf);
  if (g_pcap == NULL) {
      cout << "pcap_open_live() failed: " << errbuf << endl;
      return 1;
  }

  struct bpf_program fp;  
  if(pcap_compile(g_pcap,&fp,filter.c_str(),0,PCAP_NETMASK_UNKNOWN) == -1){
    cout << "pcap_compile error: " << filter << endl;
    return 1;  
  }
  
  if(pcap_setfilter(g_pcap,&fp) == -1){
    cout << "pcap_setfilter error: " << filter << endl;
    return 1;
  }
  
  g_pb = new PacketBuilder();
  
  signal(SIGINT,handler);

  pcap_loop(g_pcap, -1, packetHandler, NULL);

  delete g_pb;
  cout << "Have a nice day" << endl;
  return 0;
}
