/************************************
 * Herakles
 * Brief: IPbus module for python
 * 
 * Author: Daniel Bullock
 *         Carlos.Solans@cern.ch
 ************************************/

#define PY_SSIZE_T_CLEAN //Not sure we need this
#include <Python.h>
#include <stddef.h>
#include "ipbus/Uhal.h"
#include <iostream>

#if PY_VERSION_HEX < 0x020400F0

#define Py_CLEAR(op)                            \
  do {                                          \
    if (op) {                                   \
      PyObject *tmp = (PyObject *)(op);		      \
      (op) = NULL;                              \
      Py_DECREF(tmp);                           \
    }                                           \
  } while (0)

#define Py_VISIT(op)                            \
  do {                                          \
    if (op) {                                   \
      int vret = visit((PyObject *)(op), arg);	\
      if (vret)                                 \
        return vret;                            \
      }                                         \
  } while (0)

#endif //PY_VERSION_HEX < 0x020400F0


#if PY_VERSION_HEX < 0x020500F0

typedef int Py_ssize_t;
#define PY_SSIZE_T_MAX INT_MAX
#define PY_SSIZE_T_MIN INT_MIN
typedef inquiry lenfunc;
typedef intargfunc ssizeargfunc;
typedef intobjargproc ssizeobjargproc;

#endif //PY_VERSION_HEX < 0x020500F0

#ifndef PyVarObject_HEAD_INIT
#define PyVarObject_HEAD_INIT(type, size)	\
  PyObject_HEAD_INIT(type) size,
#endif

#if PY_VERSION_HEX >= 0x03000000

#define MOD_ERROR NULL
#define MOD_RETURN(val) val
#define MOD_INIT(name) PyMODINIT_FUNC PyInit_##name(void)
#define MOD_DEF(ob,name,doc,methods)                    \
  static struct PyModuleDef moduledef = {               \
    PyModuleDef_HEAD_INIT, name, doc,-1, methods	      \
  };                                                    \
  m = PyModule_Create(&moduledef);                      \

#else

#define MOD_ERROR 
#define MOD_RETURN(val) 
#define MOD_INIT(name) PyMODINIT_FUNC init##name(void)
#define MOD_DEF(ob,name,doc,methods)                 \
    ob = Py_InitModule3((char *) name, methods, doc);

#endif //PY_VERSION_HEX >= 0x03000000

/************************************
 *
 * Module declaration
 *
 ************************************/

typedef struct {
  PyObject_HEAD
  ipbus::Uhal *obj;
  std::vector<uint32_t> vec;
} PyIpbusUhal;

static int _PyIpbusUhal_init(PyIpbusUhal *self, PyObject *args)
{
    const char *connstr;
    Py_ssize_t connstr_len;
    if (!PyArg_ParseTuple(args, (char *) "s#", &connstr, &connstr_len)) {
      return -1;
    }
    self->obj = new ipbus::Uhal(std::string(connstr, connstr_len));
    return 0;
}

static void _PyIpbusUhal_dealloc(PyIpbusUhal *self)
{
  delete self->obj;
  Py_TYPE(self)->tp_free((PyObject*)self);
}

PyObject * _PyIpbusUhal_Read(PyIpbusUhal *self, PyObject *args, PyObject *kwargs)
{
  uint32_t address;
  uint32_t size=1;
  PyObject *py_fifo = NULL;
  const char *keywords[] = {"address", "size", "fifo", NULL};
  if (!PyArg_ParseTupleAndKeywords(args, kwargs, (char *) "I|IO", (char **) keywords, 
				   &address, &size, &py_fifo)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  bool fifo = py_fifo? (bool) PyObject_IsTrue(py_fifo) : false;
  self->obj->Read(address, self->vec, size, fifo);
  PyObject *py_ret;
  
  //size to read is one or greater
  if(size>1){
    py_ret = PyList_New(size); 
    for(uint32_t i=0;i<size;i++){
      PyObject * it = PyLong_FromLong(self->vec.size()>i?(long)self->vec.at(i):0);
      if(PyList_SetItem(py_ret,i,it)!=0){
        // Error parsing item
        Py_INCREF(Py_None);
        return(Py_None);
      }
    }
  }else{
    py_ret = PyLong_FromLong(self->vec.size()>0?(long)self->vec.at(0):0);
  }
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyIpbusUhal_RMWB(PyIpbusUhal *self, PyObject *args, PyObject *kwargs)
{
  uint32_t address;
  uint32_t mask;
  uint32_t value;
  uint32_t startbit;
  uint32_t nbits;
  const char *keywords2[] = {"address", "value", "startbit", "nbits", NULL};
  if(PyArg_ParseTupleAndKeywords(args, kwargs, (char *) "IIII", (char **) keywords2, 
                                 &address, &value, &startbit, &nbits)) {    
    self->obj->RMWB(address, value, startbit, nbits);
  }
  else if(PyArg_ParseTuple(args, (char *) "III", &address, &mask, &value)) {    
    self->obj->RMWB(address, mask,value);
  }
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyIpbusUhal_Write(PyIpbusUhal *self, PyObject *args, PyObject *kwargs)
{
  PyObject *py_fifo = NULL;
  PyObject *py_values = NULL;
  uint32_t address;
  uint32_t len_values;
  //(I:)# values, len
  const char *keywords2[] = {"address", "values", "fifo", NULL};
  if(PyArg_ParseTupleAndKeywords(args, kwargs, (char *) "IO|O", (char **) keywords2, 
				 &address, &py_values, &py_fifo)) {
    self->vec.clear();
    if(PyList_Check(py_values)){ //accept a python list [1,1,2]
      len_values = PyList_Size(py_values);
      for(uint32_t i=0;i<len_values;i++){
        self->vec.push_back(PyLong_AsLong(PyList_GetItem(py_values,i)));
      }
    }else if(PyTuple_Check(py_values)){ //accept a python tuple (1,1,2)
      len_values = PyTuple_Size(py_values);
      for(uint32_t i=0;i<len_values;i++){
        self->vec.push_back(PyLong_AsLong(PyTuple_GetItem(py_values,i)));
      }
    }else if(PyLong_Check(py_values)){ //accept an integer
      self->vec.push_back(PyLong_AsLong(py_values));
    }else{
      Py_INCREF(Py_None);
      return Py_None;
    }
    bool fifo = py_fifo? (bool) PyObject_IsTrue(py_fifo) : false;
    self->obj->Write(address, self->vec, fifo);
  }
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyIpbusUhal_SetTimeout(PyIpbusUhal *self, PyObject *args)
{
  uint32_t millis;
  if(PyArg_ParseTuple(args, (char *) "I",&millis)){
    self->obj->SetTimeout(millis);
  }
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyIpbusUhal_SetVerbose(PyIpbusUhal *self, PyObject *args)
{
  PyObject * enable = NULL;
  if(PyArg_ParseTuple(args, (char *) "O",&enable)){
    if(enable!=NULL){
      self->obj->SetVerbose((bool) PyObject_IsTrue(enable));
    }
  }
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyIpbusUhal_Sync(PyIpbusUhal *self)
{
  self->obj->Sync();
  PyObject * alive = Py_True;
  if(!self->obj->IsSynced()){alive = Py_False;}
  Py_INCREF(alive);
  return alive;
}

PyObject * _PyIpbusUhal_IsSynced(PyIpbusUhal *self)
{
  PyObject * alive = Py_True;
  if(!self->obj->IsSynced()){alive = Py_False;}
  Py_INCREF(alive);
  return alive;
}

PyObject * _PyIpbusUhal_GetPacketId(PyIpbusUhal *self)
{
  return PyLong_FromLong(self->obj->GetPacketId());
}

PyObject * _PyIpbusUhal_GetNumCalls(PyIpbusUhal *self)
{
  return PyLong_FromLong(self->obj->GetNumCalls());
}

static PyMethodDef PyIpbusUhal_methods[] = {
  {(char *) "Read",      (PyCFunction) _PyIpbusUhal_Read,       METH_KEYWORDS|METH_VARARGS, NULL },
  {(char *) "Write",     (PyCFunction) _PyIpbusUhal_Write,      METH_KEYWORDS|METH_VARARGS, NULL },
  {(char *) "RMWB",      (PyCFunction) _PyIpbusUhal_RMWB,       METH_KEYWORDS|METH_VARARGS, NULL },
  {(char *) "SetTimeout",(PyCFunction) _PyIpbusUhal_SetTimeout, METH_VARARGS, NULL },
  {(char *) "SetVerbose",(PyCFunction) _PyIpbusUhal_SetVerbose, METH_VARARGS, NULL },
  {(char *) "Sync",      (PyCFunction) _PyIpbusUhal_Sync,       METH_NOARGS, NULL },
  {(char *) "IsSynced",  (PyCFunction) _PyIpbusUhal_IsSynced,   METH_NOARGS, NULL },
  {(char *) "GetPacketId",(PyCFunction)_PyIpbusUhal_GetPacketId,METH_NOARGS, NULL },
  {(char *) "GetNumCalls",(PyCFunction)_PyIpbusUhal_GetNumCalls,METH_NOARGS, NULL },
  {NULL, NULL, 0, NULL}
};

PyTypeObject PyIpbusUhal_Type = {
    PyVarObject_HEAD_INIT(NULL, 0)
    (char *) "Herakles.Uhal",  /* tp_name */
    sizeof(PyIpbusUhal),       /* tp_basicsize */
    0,                         /* tp_itemsize */
    (destructor)_PyIpbusUhal_dealloc,      /* tp_dealloc */
    0,                         /* tp_print */
    0,                         /* tp_getattr */
    0,                         /* tp_setattr */
    0,                         /* tp_reserved */
    0,                         /* tp_repr */
    0,                         /* tp_as_number */
    0,                         /* tp_as_sequence */
    0,                         /* tp_as_mapping */
    0,                         /* tp_hash  */
    0,                         /* tp_call */
    0,                         /* tp_str */
    0,                         /* tp_getattro */
    0,                         /* tp_setattro */
    0,                         /* tp_as_buffer */
    Py_TPFLAGS_DEFAULT,        /* tp_flags */
    "Ipbus Uhal",              /* Documentation string */
    0,                         /* tp_traverse */
    0,                         /* tp_clear */
    0,                         /* tp_richcompare */
    0,                         /* tp_weaklistoffset */
    0,                         /* tp_iter */
    0,                         /* tp_iternext */
    PyIpbusUhal_methods,       /* tp_methods */
    (struct PyMemberDef*)0,    /* tp_members */
    0,                         /* tp_getset */
    0,                         /* tp_base */
    0,                         /* tp_dict */
    0,                         /* tp_descr_get */
    0,                         /* tp_descr_set */
    0,                         /* tp_dictoffset */
    (initproc)_PyIpbusUhal_init,         /* tp_init */
    PyType_GenericAlloc,       /* tp_alloc */
    PyType_GenericNew          /* tp_new */
};

static PyMethodDef Herakles_methods[] = {
  {NULL, NULL, 0, NULL}
};

MOD_INIT(Herakles)
{
    PyObject *m;
    MOD_DEF(m, "Herakles", NULL, Herakles_methods);
    if(PyType_Ready(&PyIpbusUhal_Type)<0){
      return MOD_ERROR;
    }
    PyModule_AddObject(m, (char *) "Uhal", (PyObject *) &PyIpbusUhal_Type);
    return MOD_RETURN(m);
}
