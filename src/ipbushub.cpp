#include <iostream>
#include <sstream>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <netinet/in.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <map>
#include <mutex>
#include <thread>
#include <unistd.h>
#include <sstream>
#include <fcntl.h>
#include "ipbus/PacketBuilder.h"
#include "ipbus/Packet.h"
#include "ipbus/Uhal.h"
#include "ipbus/PacketHeader.h"

using namespace std;
using namespace ipbus;

//Global variables
std::mutex * g_lock;
std::map<uint32_t,Uhal*> g_devices;
std::map<uint32_t,uint32_t> g_pkids;
bool g_verbose = false;
bool g_cont = false;

void handle(int){
  cout << "Stopping ipbushub" << endl;
  g_cont=false;
}

//Forward declarations
void Listen(int);

//Main function
int main (int argc, char **argv){
    
  bool verbose = false;
  uint32_t port = 50001;
  
  //Parse arguments
  for(int i=1;i<argc;i++){
	const char * pos=strstr(argv[i],"-h");
	if(pos==NULL) continue;
	cout << "Usage: " << argv[0] << " [-p port] [-v]" << endl
	     << " p port : port to listen to" << endl
	     << " v      : enable verbose mode" << endl;
	return 0;
  }

  for(int i=1;i<argc;i++){
  const char * pos=strstr(argv[i],"-p");
	if(pos==NULL) continue;
	if(strlen(argv[i])>2){
	  port = atoi(&argv[i][2]);
	}else{
	  port = atoi(argv[i+1]);
	}
  }

  for(int i=1;i<argc;i++){
	const char * pos=strstr(argv[i],"-v");
	if(pos==NULL) continue;
	verbose=true;
  }

  
  cout << "Starting ipbushub on port " << port << endl;

  cout << "Error codes: " << endl
	   << " EPIPE:      " << EPIPE << endl
	   << " ENOSTR:     " << ENOSTR << endl
	   << " ECONNRESET: " << ECONNRESET << endl
	   << " ETIMEDOUT:  " << ETIMEDOUT << endl
	   << endl;


  //Set verbose and locale 
  g_verbose=verbose;
  Uhal::SetVerbose(verbose);
  setlocale(LC_ALL, "en-US");
  
  //Init tcp receiving socket
  int sockfd = socket(AF_INET, SOCK_STREAM, 0);
  
  //Set listening socket to non-block
  //int flags=fcntl(sockfd, F_GETFL);
  //fcntl(sockfd, F_SETFL, flags | O_NONBLOCK);

  //Set reuse address
  int reuse_addr=1;
  setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (const void*)&reuse_addr, sizeof(reuse_addr));
  //Set reuse port
  int reuse_port=1;
  setsockopt(sockfd, SOL_SOCKET, SO_REUSEPORT, (const void*)&reuse_port, sizeof(reuse_port));
  //set linger on close
  struct linger onclose;
  onclose.l_onoff = 0;
  onclose.l_linger = 1;
  setsockopt(sockfd, SOL_SOCKET, SO_LINGER, (const void*)&onclose, sizeof(onclose));
  //set low latency
  int low_latency=1;
  setsockopt(sockfd, IPPROTO_TCP, 1, (char *)&low_latency, sizeof(low_latency));
  
  //bind to port
  struct sockaddr_in serv_addr;
  memset(&serv_addr, 0, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  serv_addr.sin_port = htons(port);

  int ret = ::bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr));
  if(ret<0){cout << "ERROR binding to port " << port << endl;}
  
  //Mark the socket to listen
  listen(sockfd,5);

  signal(SIGKILL,handle);
  signal(SIGTERM,handle);
  signal(SIGINT,handle);
  signal(SIGPIPE, SIG_IGN);

  g_cont=true;
  while(true){
    if(!g_cont) break;
	struct timeval tv;
	fd_set rfds;
	FD_ZERO(&rfds);
	FD_SET(sockfd,&rfds);
	tv.tv_sec = 0;
	tv.tv_usec = 100000;
	int sel = select(sockfd+1, &rfds,0,0,&tv);
	if(sel>0){
	  struct sockaddr_in cli_addr;
	  socklen_t cli_len;
	  int client = accept(sockfd, (struct sockaddr *) &cli_addr, &cli_len);
	  if(client > 0){Listen(client);}
	}
  }

  cout << "Have a nice day" << endl;
  return 0;
}

void Listen(int sock){
  cout << "Client socket open  (" << sock <<")" << endl;
  PacketBuilder pb; 
  uint32_t preamble_r[4];
  uint32_t preamble_w[4];
  bool synced=false;
  
  while(true){
    if(!g_cont) break;
	
	if(g_verbose) cout << endl;
    int nb = read(sock,preamble_r,12);
	if(g_verbose) cout << "Read preamble: " << nb << endl;
	if(nb<=0){
	  if(g_verbose) cout << "Socket error: " << errno << endl;
	  usleep(30000); 
	  if(errno==0) continue;
	  else if(errno==EPIPE) break;
	  else if(errno==ENOSTR) break;
	  else if(errno==ECONNRESET) break;
	  else if(errno==ETIMEDOUT) break;
	}
	if(g_verbose && false){
	  cout << hex 
		   << "Size: 0x" << preamble_r[0] << endl
		   << "Addr: 0x" << preamble_r[1] << endl
		   << "Port: 0x" << preamble_r[2] << endl
		   << dec;
	}
    
	uint32_t packet_length = ntohl(preamble_r[0])-8;
	uint32_t devicehost = preamble_r[1];
	uint32_t deviceport = (ntohl(preamble_r[2]) >> 16) & 0xFFFF;
	//uint32_t nbytes = (ntohl(preamble_r[2]) & 0xFF)*4;
	
	nb = read(sock,pb.GetBytes(),packet_length);
	if(g_verbose) cout << "Read data: " << nb << endl;
	if(nb<=0) {
	  if(g_verbose) cout << "Socket error: " << errno << endl;
	  break;
	}
	
	pb.SetLength(packet_length);
    pb.Unpack();
	
	map<uint32_t,Uhal*>::iterator it = g_devices.find(devicehost);
	Uhal * device=it->second;
	if(it==g_devices.end()){
	  ostringstream os;
	  os.str("");
	  os << "udp://"
		 << ((devicehost & 0x000000FF)>>0 ) << "." 
		 << ((devicehost & 0x0000FF00)>>8 ) << "." 
		 << ((devicehost & 0x00FF0000)>>16) << "." 
		 << ((devicehost & 0xFF000000)>>24) 
		 << ":" << deviceport;
	  device = new Uhal(os.str());
	  g_devices[devicehost]=device;
	  g_pkids[devicehost]=Packet::GetNextPacketId();
	}
	
	if(!synced){
	  //device->Sync();
	  g_pkids[devicehost]=Packet::GetNextPacketId();
	}

	Transaction *req = (Transaction*) pb.GetPacket();
    if(g_verbose){
	  cout << "Request to device " << endl;
	  req->Dump();
	}
	
	uint32_t pkid=req->GetPkid();
    req->SetPkid(g_pkids[devicehost]++);
	
	Packet *rep = (Packet*) device->Send(req);
	if(rep==NULL){
	  if(g_verbose) cout << "Device is down" << endl;
	  packet_length=0;
	}
	else{
	  packet_length=rep->GetLength();
	  //req->Sent();
	  if(g_verbose){
		cout << "Reply from device " << endl;
		rep->Dump();
	  }
      rep->SetPkid(pkid);
    }
	
	preamble_w[0]=htonl(packet_length+12);
	preamble_w[1]=0;
	preamble_w[2]=devicehost;
	preamble_w[3]=htons(deviceport);
	write(sock,preamble_w,16);
	if(rep){
	  write(sock,rep->GetBytes(),rep->GetLength());
	}
  }
  close(sock);
  cout << "Client socket close (" << sock <<")" << endl;
}
