#ifndef Register_h
#define Register_h 1

#include "ipbus/Uhal.h"
#include <stdint.h>
#include <string>

namespace ipbus{

  /**
   * A Register is an interface for the RMWB operation of the IPbus.
   * It is defined by a name, an address, start bit and number of bits. 
   * Its contents can be read (Register::SetValue) and write (Register::GetValue).
   * The bits not defined in the Register will be discarded.
   * The updated flag (Register::Updated) is set each time a write is 
   * performed to the Register but not forced into the device.
   *
   * @brief IPbus 2.0 R/W/M/S helper class
   * @author Carlos.Solans@cern.ch
   **/
  class Register{

    public:

    /**
     * A register is a set of bits, up to 32, that are allocated in an address
     * @brief Build a register
     * @param name register name
     * @param address register address
     * @param startbit first bit of the register
     * @param nbits number of bits of the register
     * @param defvalue register address
     **/
    Register(std::string name, uint32_t address, uint32_t startbit, uint32_t nbits, uint32_t defvalue);
    
    /**
     * @brief Delete the register
     */
    ~Register();
    
    /**
     * @brief Set the Uhal pointer in the register
     */
    void SetUhal(Uhal* uhal);
    
    /**
     * @brief Set the value of the register
     * @param value the new value for the register
     * @param force force the write to the Uhal
     */
    void SetValue(uint32_t value, bool force=false);
    
    /**
     * @brief Set the update flag of the register
     * @param updated the new value for the flag
     */
    void SetUpdated(bool updated);

    /**
     * @brief Get the value of the register
     * @param force force the read to the Uhal
     */
    uint32_t GetValue(bool force=false);
    
    /**
     * @brief Check if the register value was updated
     * @return true if the value was updated by Register::SetValue
     */
    bool Updated();
    
    /**
     * @brief Get the name of register
     */
    std::string GetName();
    
    /**
     * @brief Get the address of register
     */
    uint32_t GetAddress();
    
    /**
     * @brief Get the start bit of the register
     */
    uint32_t GetStartBit();
    
    /**
     * @brief Get the number of bits of the register
     */
    uint32_t GetNbits();
    
    /**
     * @brief Get the bit mask of the register
     */
    uint32_t GetMask();
    
    /**
     * @brief Get the default value of the register
     */
    uint32_t GetDefault();
    
  private:
    
    std::string m_name;
    uint32_t m_address;
    uint32_t m_startbit;
    uint32_t m_nbits;
    uint32_t m_defvalue;
    uint32_t m_value;
    uint32_t m_mask;
    bool m_updated;
    Uhal * m_uhal;
    
  };

}

#endif
