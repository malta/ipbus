#ifndef PacketHeader_h
#define PacketHeader_h 1

#include <stdint.h>

namespace ipbus{

  /**
   * The Packet header contains the protocol version (4 bits), 
   * the Packet ID (16 bits), the byte order qualifier (4 bits),
   * and the Packet type PacketHeader::CONTROL, PacketHeader::STATUS, 
   * or PacketHeader::RESEND (4 bits) encoded in 32 bits.
   * The byte order qualifier is always little endian (\c 0xF) 
   * for this implementation.
   * 
   * | 31 | 28 | 27 | 24 |  23 |   8 | 7 | 4 | 3 | 0 |
   * | -- | -- | -- | -- | --- | --- | - | - | - | - |
   * | Ver    || 0x0    || Packet ID|| BYOQ || Type ||  
   *
   * @brief IPbus 2.0 Packet header
   * @author Carlos.Solans@cern.ch
   **/
  class PacketHeader{
    
  private:
    uint32_t pver; //Protocol version 4 bits [28-31]
    uint32_t pkid; //Packet ID 16 bits [8-23]
    uint32_t byoq; //Byte order qualifier [4-7]
    uint32_t type; //Type ID 4 bits [0-3]
    
    uint32_t head; //encoded header

  public:

    //! Transaction Packet type
    static const uint32_t CONTROL=0; 
    //! Status Packet type
    static const uint32_t STATUS=1; 
    //! Resend Packet type
    static const uint32_t RESEND=2; 
    
    //! Byte order qualifier
    static const uint32_t BYOQ=0xF;

    //! Version
    static const uint32_t VERSION=2;

    /**
     * @brief Construct an empty PacketHeader of type 2.0 and little endian PacketHeader::BYOQ
     **/
    PacketHeader();
    
    /**
     * @brief Empty destructor
     **/
    ~PacketHeader();
    
    /**
     * @brief Dump contents of header on screen
     **/
    void Dump();

    /**
     * @brief Get the protocol version
     * @return Integer value of the protocol version. Should be 2.
     **/
    uint32_t GetPver();

    /**
     * @brief Get the Packet ID
     * @return Integer value of the Packet ID (0 to 0xFFFF).
     **/
    uint32_t GetPkid();

    /**
     * @brief Get the Packet type (PacketHeader::CONTROL, PacketHeader::STATUS, PacketHeader::RESEND)
     * @return Integer value of the Packet type
     **/
    uint32_t GetType();
    
    /**
     * @brief Get the byte order qualifier
     * @return Integer value of the byte order qualifier. Should be 0xF.
     **/
    uint32_t GetByoq();

    /**
     * @brief Get the integer value of the Packet header (32-bit).
     * @return Integer value of the Packet header.
     **/
    uint32_t GetInt();

    /**
     * @brief Get the protocol version
     * @param v Integer value of the protocol version. Typically 2.
     **/
    void SetPver(uint32_t v);

    /**
     * @brief Set the Packet ID
     * @param v Integer value of the Packet ID (0 to 0xFFFF)
     **/
    void SetPkid(uint32_t v);
    
    /**
     * @brief Set the Packet type (PacketHeader::CONTROL, PacketHeader::STATUS, PacketHeader::RESEND)
     * @param v Integer value of the Packet type
     **/
    void SetType(uint32_t v);
    
    /**
     * @brief Set the byte order qualifier
     * @param v Integer value of the byte order qualifier. Typically 0xF.
     **/
    void SetByoq(uint32_t v);

    /**
     * @brief Set the integer value of the Packet header (32-bit).
     * @param v Integer value of the Packet header.
     **/
    void SetInt(uint32_t v);
    
    /**
     * @brief Read the Packet header from a byte array b starting at position pos
     * @param b Byte array.
     * @param pos Starting position.
     **/
    void SetBytes(uint8_t * b, uint32_t pos);

  private:
    //Convert beans into head
    void Pack();

    //Convert head into beans
    void Unpack();

    
  };
}

#endif
