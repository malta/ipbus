#ifndef PacketBuilder_h
#define PacketBuilder_h 1

#include "ipbus/Packet.h"
#include "ipbus/PacketHeader.h"
#include <cstdlib>

namespace ipbus{

  /**
   * PacketBuilder is a tool that takes the bytes read by a socket
   * and builds an IPbus Packet out of them of the type Transaction,
   * Status, or Resend.
   * The maximum number of bytes allowed in a Packet is 1472.
   * 
   * @brief Tool to build a Packet from its bytes
   * @author Carlos.Solans@cern.ch
   **/
  class PacketBuilder{

  private:
    
    PacketHeader* header;
    uint8_t* bytes;
    uint32_t length;
    Packet * transaction;
    Packet * resend;
    Packet * status;

  public:
    
    /**
     * @brief Allocate the maximum number of bytes and initialize all Packet types
     */
    PacketBuilder();
    
    /**
     * @brief Free the allocated memory and delete the all Packet types
     */
    ~PacketBuilder();
			     
    /**
     * @brief Get the buffer used to build a packet
     * @return Pointer to the byte array
     **/
    uint8_t* GetBytes();

    /**
     * @brief Set the length of the buffer
     * @param v The lenth of the buffer
     **/
    void SetLength(uint32_t v);

    /**
     * @brief Print the bytes through std output
     **/
    void Dump();

    /**
     * @brief Decode the bytes into a Packet
     **/
    void Unpack();
    
    /**
     * @brief Get the Packet header 
     * @return The PacketHeader object
     **/
    PacketHeader * GetHeader();
    
    /**
     * @brief Check the consistency of the Packet
     **/
    bool CheckHeader();

    /**
     * @brief Get the decoded Packet (Transaction, Status, or Resend)
     * @return The decoded Packet object
     **/
    const Packet * GetPacket();
    
  };
}

#endif
