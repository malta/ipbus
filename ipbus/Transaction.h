#ifndef Transaction_h
#define Transaction_h 1

#include "ipbus/Packet.h"
#include "ipbus/TransactionHeader.h"
#include <stdint.h>
#include <vector>

namespace ipbus{

  /**
   * A Transaction is a type of Packet that request an action on an IPBus address.
   * The possible Transactions are 
   * to read one or many consecutive addresses (TransactionHeader::READ), 
   * read one or many values from a single address (TransactionHeader::READFIFO),
   * write one or many consecutive addresses (TransactionHeader::WRITE), 
   * write one or many values from a single address (TransactionHeader::WRITEFIFO),
   * or write few bits from a single address (TransactionHeader::RMWB).
   * Transaction Packets can be built as a REQUEST or as a RESPONSE.
   * The RESPONSE Transaction contains the values of the registers.
   * 
   * @brief IPbus 2.0 Transaction Packet
   * @author Carlos.Solans@cern.ch
   **/

  class Transaction: public Packet{

  private:
    
    TransactionHeader * theader;  //! the transaction header
    uint32_t address;             //! the transaction address
    std::vector<uint32_t> values; //! vector of read values

  public:
      
    /**
     * @brief Build a transaction
     **/
    Transaction();
  
    /**
     * @brief Delete the transaction
     **/
    virtual ~Transaction();
        
    /**
     * @brief Get the transaction type (R,W,M,S)
     * Possible transaction types are 
     * TransactionHeader::READ
     * TransactionHeader::WRITE
     * TransactionHeader::READFIFO
     * TransactionHeader::WRITEFIFO
     **/
    uint32_t GetTransactionType();
    
    /**
     * @brief Get the header of the transaction
     **/
    TransactionHeader* GetTransactionHeader();

    /**
     * @brief Set the size of the transaction
     **/
    void SetSize(uint32_t size);

    /**
     * @brief Get the size of the transaction
     * @return Size of the transaction in words
     **/
    uint32_t GetSize();
    
    /**
     * @brief Set the starting address for the transaction
     **/
    void SetAddress(uint32_t addr);

    /**
     * @brief Get the starting address for the transaction
     * @return the address for the transaction
     **/
    uint32_t GetAddress();
    
    /**
     * @brief Get the data
     * @return vector of values
     **/
    const std::vector<uint32_t>& GetData() const;

    /**
     * @brief Get the data
     * @param i index of the data vector
     * @return the value at index i
     **/
    uint32_t GetData(uint32_t i);
    
    /**
     * @brief Set the data
     * @param i index of the data vector
     * @param v value for the data vector
     **/
    void SetData(uint32_t i,uint32_t v);
		
    /**
     * @brief Clear the data vector
		 **/
    void Clear();
		
    /**
     * @brief Build a read transaction
     * @param address the starting address
     * @param size number of words to read
     * @param fifo if the register is a FIFO
     **/
    void SetRead(uint32_t address, uint32_t size, bool fifo=false);
      
    /**
     * @brief Build a single register single value write transaction
     * @param address the starting address
     * @param value the value to write
     **/
    void SetWrite(uint32_t address, uint32_t value);
    
    /**
     * @brief Build write transaction of many words
     * @param address the starting address
     * @param values pointer to the values to write
     * @param size number of words to write
     * @param fifo if the register is a FIFO
     **/
    void SetWrite(uint32_t address, const uint32_t * values, uint32_t size, bool fifo=false);

    /**
     * @brief Build write transaction of many words
     * @param address the starting address
     * @param values vector of values to write
     * @param fifo if the register is a FIFO
     **/
    void SetWrite(uint32_t address, std::vector<uint32_t> values, bool fifo=false);

    /**
     * @brief Build RMW transaction as X=(X&A)|B
     * @param address the starting address
     * @param a value to and to the current value
     * @param b value to or after the and operation
     **/
    void SetRMWB(uint32_t address, uint32_t a, uint32_t b);

    /**
     * @brief Write nbits starting at startbit of value in address  
     * @param address the starting address
     * @param value value to write starting at startbit
     * @param startbit first bit to write
     * @param nbits number of bits to write
     **/
    void SetRMWB(uint32_t address, uint32_t value, uint32_t startbit, uint32_t nbits);

    /** 
     * @brief Inform this packet was sent
     * Increment the packet id 
     **/
    void Sent();

    /**
     * @brief Add values to byte stream
     * @param bytes byte array
     * @param pos a first element of the byte array
     * @return number of bytes processed
     **/
    uint32_t AddBytes(uint8_t* bytes, uint32_t pos);

    /**
     * @brief Parse the byte stream
     * @param bytes byte array
     * @param pos a first element of the byte array
     * @return number of bytes processed
     **/
    uint32_t SetBytes(uint8_t* bytes, uint32_t pos);

  };
}

#endif
