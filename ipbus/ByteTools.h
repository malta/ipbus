#ifndef ByteTools_h
#define ByteTools_h 1

#include <stdint.h>

namespace ipbus{

  /**
   * @brief Useful byte array packing tools
   * @author Carlos.Solans@cern.ch
   **/
  class ByteTools{

  private:
  
    ByteTools(){}
    ~ByteTools(){}

  public:
    
    /**
     * @brief Add the bytes of src into dest at position pos
     * @return The new position
     * @param dest Byte array where to add the bytes
     * @param pos Position of the byte array where to start
     * @param src Integer with the bytes to add
     **/
    static uint32_t AddBytes(uint8_t* dest, uint32_t pos, uint32_t src);

    /**
     * @brief Pack 4 bytes of b starting at position pos into an integer
     * @return The new integer
     * @param b Byte array that contains the bytes to pack
     * @param pos Position of the byte array where to start
     **/
    static uint32_t Pack(uint8_t * b, uint32_t pos);
    
    /**
     * @brief Dump on screen length bytes of b starting at position pos
     * @param b Byte array where to add the bytes
     * @param pos Position of the byte array where to start
     * @param length The number of bytes to dump
     **/
    static void DumpBytes(uint8_t * b, uint32_t pos, uint32_t length);
  
  };

}

#endif
