#ifndef Packet_h
#define Packet_h 1

#include "ipbus/ByteTools.h"
#include "ipbus/PacketHeader.h"

namespace ipbus{

  /**
   * A Packet is the basic structure used for the communication in IPbus.
   * It can contain a request from the Uhal or a reply from the FPGA device.
   * A Packet contains a unique packet ID, a header that identifies the Packet,
   * and a payload that can be of variable size depending on the type of the
   * Packet.
   *
   * @brief Abstract base class for IPbus 2.0
   * @author Carlos.Solans@cern.ch
   **/
  class Packet{
  
  private:
      
    static uint32_t m_pkid;
    PacketHeader * header;
    uint8_t * bytes;
    uint32_t length;
    
  public:
    
    /**
     * @brief Default Packet constructor.
     * Allocate data bytes and initialize the header with next expected packet ID
     */
    Packet();
    
    /**
     * Destructor
     */
    virtual ~Packet();
    
    /**
     * @brief Set the packet ID to the next expected packet ID
     */
    void Init();
    
    /**
     * @brief Get the packet ID
     */
    uint32_t GetPkid() const;
    
    /**
     * @brief Get the packet type
     */
    uint32_t GetType() const;
    
    /**
     * @brief Set the packet ID
     * @param pkid The packet ID
     */
    void SetPkid(uint32_t pkid);
    
    /**
     * @brief Set the packet type
     * @param type The packet type
     */
    void SetType(uint32_t type);
    
    /**
     * @brief Get the packet length
     * @return the packet length in bytes
     **/
    uint32_t GetLength();

    /**
     * @brief Set the packet length
     * @param v the new length
     **/
    void SetLength(uint32_t v);
    
    /**
     * @brief Print the packet contents through std out
     **/
    void Dump() const;
    
    /**
     * @brief Set the next expected packet ID.
     * @param pkid a packet ID
     * Next created or initialized packet will use this ID.
     **/
    static void SetNextPacketId(uint32_t pkid);

    /**
     * @brief Get the next expected packet ID.
     * @return a packet ID
     * Get the ID of the next created or initialized packet.
     **/
    static uint32_t GetNextPacketId();
    
    /**
     * @brief Increment by one count the next packet ID.
     * Commodity function to increment the next packet ID programatically.
     **/
    static void IncrementNextPacketId();

    /** 
     * @brief Callback method for child classes upon packet send
     * Increment the packet id in child classes 
     **/
    virtual void Sent()=0;

    /**
     * @brief Add values to byte stream
     * @param bytes byte array
     * @param pos a first element of the byte array
     * @return number of bytes processed
     **/
    virtual uint32_t AddBytes(uint8_t* bytes, uint32_t pos)=0;
    
    /**
     * @brief Parse the byte stream
     * @param bytes byte array
     * @param pos a first element of the byte array
     * @return number of bytes processed
     **/
    virtual uint32_t SetBytes(uint8_t* bytes, uint32_t pos)=0;

    /**
     * @brief Get packet as a byte array
     * @return byte array
     **/
    uint8_t* GetBytes();

    /**
     * @brief Set the contents from bytes
     * @param bytes array of bytes
     * @param length number of bytes to copy
     **/
    void CopyBytes(uint8_t* bytes, uint32_t length);

    /**
     * @brief Pack the objects into the stream
     **/
    void Pack();

    /**
     * @brief Unpack the stream into fields
     **/
    void Unpack();

  };
}

#endif
