#ifndef ipbus_server_h
#define ipbus_server_h 1

#include "ipbus/PacketBuilder.h"
#include "ipbus/Transaction.h"
#include <cstdlib>
#include <map>
#include <functional>
#include <thread>

namespace ipbus{

  /**
   * Server is an emulator of a device that runs IPbus over UDP.
   * The communication protocol with the Server is only UDP.
   * The Server has registers that can be read only, write only, or read-write.
   * An ipbus::Uhal client can connect to the Server and perform
   * any TransactionHeader::READ, TransactionHeader::WRITE,
   * TransactionHeader::READFIFO, TransactionHeader::WRITEFIFO,
   * TransactionHeader::RMWBITS operation on a given register.
   * The actions performed by the Server on a given register 
   * are defined by callback functions associated to the register 
   * by Server::AddRegisterRead, Server::AddRegisterWrite, and
   * Server::AddRegister.
   * 
   * For convenience, the server implements a thread that handles
   * the requests in Server::Run which is controlled by Server::Start 
   * and Server::Stop.
   *
   * @verbatim

   Server * server = new Server(port);
   ...
   server->Start();  
   while(true){...}
  
   server->Stop();
   delete server;
   
   @endverbatim
   *
   * The callback function for reading requires no 
   * parameter and returns a uint32_t value (\c uint32_t(void)).
   * For FIFO registers the function can accept a uint32_t parameter
   * to indicate the position and return a uint32_t value 
   * (\c uint32_t(uint32_t) ).
   * The callback function for writing does not return a value, 
   * and accepts one uint32_t parameter with the value to write
   * (\c void(uint32_t) ). 
   * For FIFO registers the function can accept an additional 
   * uint32_t parameter to indicate the position. In this case,
   * the first parameter is the position, and the second one 
   * the value to write (\c void(uint32_t,uint32_t) ).
   *
   * @verbatim
   
   vector<uint32_t> mem;

   srv->AddRegisterRead(0,[&](){return mem[0];});
   srv->AddRegisterWrite(0,[&](uint32_t value){mem[0]=value;});

   srv->AddRegisterRead(0,[&](uint32_t pos){return mem[pos];});
   srv->AddRegisterWrite(0,[&](uint32_t pos, uint32 value){mem[pos]=value;});

   @endverbatim
   *
   * @brief IPbus 2.0 software UDP server
   * @author Carlos.Solans@cern.ch
   **/
   
  class Server{

  public:

    /**
     * @brief Create a new UDP IPbus device emulator
     * @param port the port number for the server
     **/
    Server(uint32_t port);

    /**
     * @brief Clear the callbacks and delete the device emulator
     **/
    ~Server();

    /**
     * @brief Add a register to the device emulator
     * @param address the address for the register
     * @param rc the callback function to read the register
     * @param wc the callback function to write the register  
     **/
    void AddRegister(uint32_t address, std::function<uint32_t(void)> rc, std::function<void(uint32_t)> wc);

    /**
     * @brief Add a register to the device emulator
     * @param address the address for the register
     * @param rc the callback function to read the register uint32_t(uint32_t pos)
     * @param wc the callback function to read the register value(uint32_t pos, uint32_t value)
     **/
    void AddRegister(uint32_t address, std::function<uint32_t(uint32_t)> rc, std::function<void(uint32_t,uint32_t)> wc);

    /**
     * @brief Add a read callback to the device emulator
     * @param address the address for the register
     * @param rc the callback function to read the register uint32()
     **/
    void AddRegisterRead(uint32_t address, std::function<uint32_t(void)> rc);

    /**
     * @brief Add a read callback with position to the device emulator
     * @param address the address for the register
     * @param rc the callback function to read the register uint32_t(uint32_t pos)
     **/
    void AddRegisterRead(uint32_t address, std::function<uint32_t(uint32_t pos)> rc);

    /**
     * @brief Add a write callback to the device emulator
     * @param address the address for the register
     * @param wc the callback function to read the register void(uint32_t value)
     **/
    void AddRegisterWrite(uint32_t address, std::function<void(uint32_t)> wc);
    
    /**
     * @brief Add a write callback with position to the device emulator
     * @param address the address for the register
     * @param wc the callback function to read the register value(uint32_t pos, uint32_t value)
     **/
    void AddRegisterWrite(uint32_t address, std::function<void(uint32_t pos,uint32_t val)> wc);

    /**
     * @brief Listen to IPbus requests, access registers and return responses
     **/
    void Run();

    /**
     * @brief Start the server
     **/
    void Start();

    /**
     * @brief Stop the server
     **/
    void Stop();

  private:

    bool m_verbose;

    bool m_cont;

    PacketBuilder * m_pb;

    Transaction * m_trans;

    int m_sockfd;

    uint32_t m_pkid;
    

    std::map<uint32_t, std::function<uint32_t(void)> > m_rcs;

    std::map<uint32_t, std::function<uint32_t(uint32_t)> > m_rfs;

    std::map<uint32_t, std::function<void(uint32_t)> > m_wcs;

    std::map<uint32_t, std::function<void(uint32_t,uint32_t)> > m_wfs;

    std::thread m_thread;
    
  };

}

#endif
