#ifndef Resend_h
#define Resend_h 1

#include "ipbus/Packet.h"
#include <stdint.h>

namespace ipbus{

  /**
   * Resend requests a Packet to be re-sent from the device by its Packet ID.
   * Requires the Packet ID (Packet::SetPkid) to be defined to identify the 
   * Packet that needs to be re-sent.
   *
   * @brief IPbus 2.0 Resend Packet
   * @author Carlos.Solans@cern.ch
   **/
  class Resend: public Packet{
    
  public:
    
    /**
     * @brief Constructor
     * Set packet type and id
     **/
    Resend();
    
    /**
     * @brief Destructor
     **/
    ~Resend();

    /**
     * @brief This package was sent
     **/
    void Sent();

    /**
     * @brief Add values to byte stream
     * @param bytes byte array
     * @param pos a first element of the byte array
     * @return number of bytes processed
     **/
    uint32_t AddBytes(uint8_t* bytes, uint32_t pos);
    
    /**
     * @brief Parse byte stream
     * @param bytes byte array
     * @param pos a first element of the byte array
     * @return number of bytes processed
     **/
    uint32_t SetBytes(uint8_t* bytes, uint32_t pos);
    
  };
}

#endif
