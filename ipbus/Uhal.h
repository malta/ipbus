#ifndef Uhal_h
#define Uhal_h 1

#include "ipbus/PacketBuilder.h"
#include "ipbus/Packet.h"
#include "ipbus/Transaction.h"
#include "ipbus/Status.h"
#include "ipbus/Resend.h"
#include <cstdlib>
#include <string>
#include <vector>
#include <netdb.h>

namespace ipbus{

  /**
   * Uhal is a lightweight IPbus client to communicate with an Ipbus Server.
   * It requires a connection string of the format 
   * "protocol://hostname:port?target=device:port", where the available
   * protocols are direct access (UDP), or relayed through a hub (TCP).
   * Upon creation, Uhal sets the commnunicaiton protocol from the 
   * connection string, and establishes communication to the device (Uhal::Sync). 
   * The status of the connection can be retrieved anytime with Uhal::IsSynced.
   * Uhal allows any type of Packet (PacketHeader::CONTROL, PacketHeader::STATUS,
   * PacketHeader::RESEND) to be sent (Uhal::Send).
   * And the underlaying Packet ID is automatically updated on the Packet 
   * to be sent from the next expected Packet ID 
   * (Status::GetNextExpectedPacketId) that is local to the Uhal.
   * This requires that only one instance of a Uhal should be used to communicate
   * to one device at each time.
   * 
   * @brief IPbus 2.0 client
   * @author Carlos.Solans@cern.ch
   * @date March 2014 - First version
   * @date May 2018 - Remove the PacketID singleton
   **/
   
  class Uhal{

  private:

    struct sockaddr_in server;
    uint32_t devicehost;
    uint32_t deviceport;
    int32_t sock;
    PacketBuilder pb;
    static bool verbose;
    Transaction req;
    Status status;
    Resend resend;
    bool synced;
    bool connected;
    uint32_t proto;
    uint32_t * preamble;
    uint32_t pkid;
    uint32_t ncalls;
    
  public:

    //! Type of commnunication through UDP
    static const uint32_t UDP=0;
    
    //! Type of commnunication through TCP
    static const uint32_t TCP=1;

    /**
     * Resolve the host with gethostbyname, create a UDP socket,
     * disable Nagle's algorithm, set the timeout to 3 seconds,
     * and syncrhonize with the server. 
     * @param hostname host name or ip address of the server
     * @param port port number on the server
     * @brief Create Uhal::UDP Ipbus client to connect to given hostname and port.
     */
    Uhal(std::string hostname, uint32_t port);
    
    /**
     * Create a new Uhal object of type provided in the connection string
     * "protocol://hostname:port?target=device:port". If no protocol
     * is provided Uhal::UDP type is used. If no port is provided, port
     * 50001 is used. If type Uhal::TCP is requested, then it is mandatory
     * to provide the target device and port.
     * The steps are the following: parse the connection string, 
     * resolve the host with gethostbyname, create the socket,
     * disable Nagle's algorithm, set the timeout to 3 seconds,
     * and syncrhonize with the server.
     * @param conn_str connection string "protocol://host:port?target=device:port"
     * @brief Create an Ipbus client with the details from conn_str.
     */
    Uhal(std::string conn_str);
    
    /**
     * @brief Close the socket
     */
    ~Uhal();
    
    /**
     * @brief Syncronize with server by requesting the next expected packet id 
     *        from the server through a status request.
     */
    void Sync();

    /**
     * @brief Resyncronize with server by trying to recover lost packet.
     * @param t transaction to resync
     * @return Transaction resynced transaction
     */
    const Transaction * Resync(const Transaction * t);

    /**
     * @brief Send a packet to server and receive response
     * @param p packet to send to server
     * @return a packet with the response from the server
     */
    const Packet * Send(Packet * p);

    /**
     * @brief Read Modify Write Bits on an address according to X <= (X & mask) | value
     * @param address address where to write from 0 to max unsigned integer 
     * @param mask mask to apply to the register before write operation
     * @param value value to write from 0 to max unsigned integer 
     */
    void RMWB(const uint32_t address, const uint32_t mask, const uint32_t value);

    /**
     * @brief Read Modify Write Bits on an address
     * @param address address where to write from 0 to max unsigned integer 
     * @param value value to write from 0 to max unsigned integer 
     * @param startbit first bit to write to 
     * @param nbits maximum number of bits to write
     */
    void RMWB(const uint32_t address, const uint32_t value, const uint32_t startbit, const uint32_t nbits);
    
    /**
     * @brief Write a value into an address
     * @param address address where to write from 0 to max unsigned integer 
     * @param value value to write from 0 to max unsigned integer 
     */
    void Write(const uint32_t address, const uint32_t value);

    /**
     * @param address where to write
     * @param values reference to a vector containing the values to be written
     * @param size number of words to be written
     * @param fifo if enabled write always to the same register, else write to consecutive ones
     * @brief Write an array of values into a sequence of registers starting from address (fifo=false)
     *        or write the array of values into the same address (fifo=true).
     */
    void Write(const uint32_t address, const uint32_t * values, const uint32_t size, bool fifo=false);

    /**
     * @param address where to write
     * @param values reference to a vector containing the values to be written
     * @param fifo if enabled write always to the same register, else write to consecutive ones
     * @brief Write a vector of values into a sequence of registers starting from address (fifo=false)
     *        or write the vector of values into the same address (fifo=true).
     */
    void Write(const uint32_t address, const std::vector<uint32_t> & values, bool fifo=false);

    /**
     * @param address where to read from
     * @param value reference to the unsigned integer that will store the value read
     * @brief Read once from one register and return value through reference.
     */
    void Read(const uint32_t address, uint32_t & value);

    /**
     * @param address where to read from 
     * @param values pointer to array of unsigned integers that will contain the values read
     * @param size number of values to read
     * @param fifo if enabled read always from the same register, else read from consecutive ones
     * @brief Read multiple values into one array provided as a reference without memory allocation checks.
     *        Read from a sequence of registers (fifo=false) 
     *        or read a sequence from a single register (fifo=true)
     */
    void Read(const uint32_t address, uint32_t * values, const uint32_t size, bool fifo=false);

    /**
     * @param address where to read from 
     * @param values reference to vector of unsigned integers that will contain the values read
     * @param size number of values to read
     * @param fifo if enabled read always from the same register, else read from consecutive ones
     * @brief Read multiple values into a vector provided as a reference.
     *        Read from a sequence of registers (fifo=false) 
     *        or read a sequence from a single register (fifo=true).
     */
    void Read(const uint32_t address, std::vector<uint32_t> & values, const uint32_t size, bool fifo=false);

    /**
     * @param enable enable verbose mode
     * @brief Set the client to verbose or not. 
     *        Static function that can be called with no object Uhal::SetVerbose(true)
     */
    static void SetVerbose(bool enable);

    /**
     * @param millis time in milliseconds
     * @brief Set the time in milliseconds to wait for packet to arrive before timeout
     */
    void SetTimeout(uint32_t millis);

    /**
     * @brief Check if the client is synchronized
     * @return bool true if last Sync request received a reply
     */
    bool IsSynced();

    /**
     * @brief Get the number of packetid requests
     * @return The number of times a request was made that changes the packetid
     */
    uint32_t GetNumCalls();

    /**
     * @brief Get the current packet id
     * @return The current packet id value
     */
    uint32_t GetPacketId();

  };
}

#endif
